# Regras de segurança - Android

Ao trabalhar com projetos Android da Ilhasoft, o desenvolvedor deve ficar atento na utilização de chaves e garantir que elas sejam salvas em um local seguro e de forma alguma expostas publicamente. Outros itens de segurança devem também serem levados em consideração. Segue abaixo as regras para manter em segurança as chaves de assinatura do `APK` (keystore) e outras regras de segurança.

## Regras para assinatura de aplicativos (keystore)

Normalmente ao desenvolver um aplicativo Android em determinado momento é necessário a geração de uma chave para assinatura do `APK` que será distribuído para os usuários.  A geração e armazenamento remoto dessa chave é responsabilidade do gerente de desenvolvimento Android. A utilização dessa chave para geração de versão de release ou para testes pode ser feita pelo desenvolvedor, contanto que siga as seguintes regras.

Quando utilizado localmente deve ser colocado as credenciais no arquivo `local.properties`, que deve ser ignorado do versionamento através do arquivo `.gitignore`. Esse arquivo contendo as credenciais deve ficar da seguinte forma:

```
ndk.dir=/Users/name/Library/Android/sdk/ndk-bundle
sdk.dir=/Users/name/Library/Android/sdk

signature.keyAlias=alias
signature.keyPassword=12345678
signature.storeFile=/Users/name/Documents/key.jks
signature.storePassword=12345678
```

Dentro do arquivo `build.gradle` pode ser utilizado assim:

```
Properties localProperties = new Properties()
localProperties.load(project.rootProject.file('local.properties').newDataInputStream())

android {
    signingConfigs {
        signature {
            keyAlias localProperties.getProperty('signature.keyAlias')
            keyPassword localProperties.getProperty('signature.keyPassword')
            storeFile file(localProperties.getProperty('signature.storeFile'))
            storePassword localProperties.getProperty('signature.storePassword')
        }
    }
    ...
```
A chave e as credenciais não podem mais ser armazenadas juntas do projeto dentro do próprio repositório, nem para projetos públicos nem privados.

## Regras de segurança

Abaixo segue as recomendações que todos os projetos que saem da Ilhasoft devem seguir para se previnir de falhas de segurança e etc.

###Sempre ativar o ProGuard

Todos os projetos que saem da Ilhasoft devem estar com o ProGuard ativado para ofuscação do código desenvolvido e a dificultação da extração de dados sensíveis das aplicações.

Alguns links que podem ser úteis ao utilizar o ProGuard:

 - [Shrink Your Code and Resources](https://developer.android.com/studio/build/shrink-code.html)
 - [ProGuard Snippets](https://github.com/krschultz/android-proguard-snippets)

###Implementar comunicação com serviços sensíveis no lado do servidor

Sempre que for necessário criar uma lógica que esteja propensa à invasão e a prejuízo aos clientes, deve ser implementada do lado servidor, diminuindo assim a disponibilização de chaves dentro do aplicativo.

### Utilização de regras de acesso (ACLs) do Parse

Utilizar controle de acesso para garantir a segurança de dados sensíveis do Parse. (Mais informações [clique aqui](https://parseplatform.github.io/docs/android/guide/#security-for-other-objects))

### Chaves utilizadas dentro do projeto

Apesar da regra de segurança principal ser diminuir ao máximo a quantidade de chaves de API dentro do aplicativo, ainda assim muitas vezes se faz necessário. Nesses casos as chaves de debug e release não podem de forma alguma se tornarem públicas na internet, nem por meio da disponibilização explícita, nem pela publicação em repositórios open source.