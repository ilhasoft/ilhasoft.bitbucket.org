# Criação de Botões - Android

Ao trabalhar com botões estilizados no Android, o desenvolvedor deve seguir essa guideline para a obtenção de um código mais limpo e organizado, e com resultado satisfatório para todas as verões (Api 16 a Api 25).

## XMLs necessários:

Será necessário a criação de dois xmls no package drawable com o mesmo nome porém versões diferentes (v21 e outro para verões anteriores - para isto basta copiar e colar o arquivo dentro do drawable e alterar somente o diretório, inserindo -v21, por exemplo: /../.../..../drawable-v21).

### XML para versões anteriores a v21:

O xml para versões anteriores a v21 deve conter <layer-list> que tem como função exibir uma <shape> inicialmente e quando selecionado exibir outro <shape> com uma animação, como mostra o exemplo abaixo:

```
<layer-list xmlns:android="http://schemas.android.com/apk/res/android">
    <item>
        <shape android:shape="rectangle">
            <solid android:color="@color/colorPrimary"/>
            <corners android:radius="10dp"/>
        </shape>
    </item>
    <item>
        <selector android:exitFadeDuration="400" android:enterFadeDuration="400">
            <item android:state_pressed="true">
                <shape android:shape="rectangle">
                    <solid android:color="@color/colorPrimaryDark"/>
                    <corners android:radius="10dp"/>
                </shape>
            </item>
        </selector>
    </item>
</layer-list>
``` 
### XML para a v21 ou superiores:

Já o xml para versão v21 ou superiores deve conter a tag <ripple> que é responsável por reproduzir o efeito padrão do click no Android sobre o botão estilizado. Como segue:

```
<ripple xmlns:android="http://schemas.android.com/apk/res/android"
    android:color="@color/colorPrimaryDark">
    <item>
        <shape android:shape="rectangle">
            <solid android:color="@color/colorPrimary"/>
            <corners android:radius="10dp"/>
        </shape>
    </item>
</ripple>
```

## O layout do botão:

Com os xmls acima criados e organizados no package drawable, basta setar o background do botão com o nome atribuido (o mesmo para as duas versões), por exemplo:

```
<Button
	...
    android:background="@drawable/button_blue"
    ...
     />
```

*Nota: Se faz necessário a criação de um botão para cada cor/estilo que se desejar.*
