# Diagnósticos

## Warnings do Compilador

Habilite todos os warnings do compilador e os trate como se fossem erros - [vai valer a pena](https://speakerdeck.com/hasseg/the-compiler-is-your-friend).

Para tratar warnings como erros em Swift, adicione `-warnings-as-errors` às configurações de compilação.

## Clang Static Analyzer

O compilador CLang (utilizado pelo Xcode) tem um analisador estático que analisa o fluxo de dados e de controle do seu código e detecta inúmeros erros que o compilador não é capaz.

Você pode executar manualmente o analisador a partir do item de menu no Xcode: _Product -> Analyze_.

O analisador consegue trabalha em dois modos diferentes: `swallow` e `deep`. O modo `deep` é executado mais demoradamente que o modo `swallow` por conta de sua varredura profunda.

Recomendações:

- Habilite todos os checks do analisador.
- Habilite o `Analize during 'Build'` na configuração de compilação de release para que o analisador seja executado automaticamente após a compilação de releases.
- Configure o campo `Mode of Analysis for 'Analysis'` da configuração de compilação para `shallow`.
- Configure o campo `Mode of Analysis for 'Build'` da configuração de compilação para `deep`.

## Debugging

Quando seu app falha o Xcode não abre o debugger por padrão. Para que isto aconteça, adicione um breakpoint de exceção (clique no "+" no fundo da Navigator de breakpoints do Xcode) para interromper a execução sempre que uma exceção for lançada. Isto irá capturar qualquer exceção, até mesmo as que já são tratadas.

## Profiling

O Xcode vem com uma suite de análise de perfomance chamada `Instruments`. Ela contém uma infinidade de ferramenta para análise de uso de memória, cpu, rede, gráficos e muito mais. É de uma complexidade só! Mas um de seus casos mais simples é buscar `memory leaks` utilizando o instrumento `Allocations`: aperte o botão `Record` e filtre o sumário em alguma string útil, como o prefixo do nome das classes do seu app. A contagem da coluna `Persistence` te diz quantas instâncias de cada objeto você tem. Qualquer classe a que mostre o aumento indiscriminado na contagem indica `memory leaking`.  
