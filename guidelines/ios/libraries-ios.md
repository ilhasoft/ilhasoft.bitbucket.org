# Bibliotecas mais utilizadas

- [Alamofire](https://github.com/Alamofire/Alamofire) - Alamofire is an HTTP networking library written in Swift.
- [Object Mapper](https://github.com/Hearst-DD/ObjectMapper) - ObjectMapper is a framework written in Swift that makes it easy for you to convert your model objects (classes and structs) to and from JSON.
- [RxSwift](https://github.com/ReactiveX/RxSwift) - Reactive Programming in Swift.
- [ReactiveCocoa](https://github.com/ReactiveCocoa/ReactiveCocoa) - Streams of values over time.
- [Parse SDK](https://github.com/parse-community/Parse-SDK-iOS-OSX) - Parse SDK for iOS/OS X/watchOS/tvOS.
- [Firebase SDK](https://github.com/firebase/firebase-ios-sdk) - Firebase iOS SDK.
- [Realm](https://github.com/realm/realm-cocoa) - Realm is a mobile database: a replacement for Core Data & SQLite.
- [SDWebImage](https://github.com/rs/SDWebImage) - Asynchronous image downloader with cache support as a UIImageView category
- [KingFisher](https://github.com/onevcat/Kingfisher) - A lightweight, pure-Swift library for downloading and caching images from the web.
- [ISOnDemandTableView](https://github.com/Ilhasoft/ISOnDemandTableView) - Load your TableView content dynamically as you scroll.
- [Spring](https://github.com/MengTo/Spring) -  A library to simplify iOS animations in Swift.
- [TPKeyboardAvoiding](https://github.com/michaeltyson/TPKeyboardAvoiding) - A drop-in universal solution for moving text fields out of the way of the keyboard in iOS.
- [SwipeCellKit](https://github.com/SwipeCellKit/SwipeCellKit) - Swipeable UITableViewCell based on the stock Mail.app, implemented in Swift.
- [GrowingTextView](https://github.com/KennethTsang/GrowingTextView) - An UITextView in Swift3, Swift4 and Swift2.3. Support auto growing, placeholder and length limit.
- [SnapKit](https://github.com/SnapKit/SnapKit) - A Swift Autolayout DSL for iOS & OS X.
- [CleanroomLogger](https://github.com/emaloney/CleanroomLogger) - CleanroomLogger provides an extensible Swift-based logging API that is simple, lightweight and performant.
