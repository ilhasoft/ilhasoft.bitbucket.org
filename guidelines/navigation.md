[![](logo-ilhasoft.png)](index.md)

[Controle de versão](version-control.md)

[Android]()

  * [Bibliotecas](android/libraries-android.md)
  * [Boas práticas](android/coding-android.md)
  * [Criação de botões](android/custom-buttons-android.md)
  * [Gerenciamento de Chaves](android/keys-management.md)

[iOS]()

  * [Analytics](ios/analytics-ios.md)
  * [Bibliotecas](ios/libraries-ios.md)
  * [Boas práticas](ios/coding-ios.md)
  * [Deployment](ios/deployment-ios.md)
  * [Diagnósticos](ios/diagnostics-ios.md)
  * [In-App Purchases (IAP)](ios/in-app-purchases-ios.md)

[Artigos](articles.md)

[gimmick:theme](flatly)
[gimmick:ThemeChooser](Mudar tema)
