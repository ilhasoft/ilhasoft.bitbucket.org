# Controle de versão

## Introdução

Usamos o **Gitflow** como nosso _workflow_ no que diz respeito ao controle de versão dos nossos projetos.

Nesta página você encontrará um pequeno resumo ~~detonado~~ para acesso rápido dos comandos que podem ser executados pela linha de comando.

Para referências mais completas sobre como trabalhar com **Gitflow** sugiro que leia as seguintes referências: [A successful Git branching model](http://nvie.com/posts/a-successful-git-branching-model/), [Atlassian - Gitflow workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow).
Segue o link para um conjunto de extensões que ajudam no manuseio do **Gitflow** pela linha de comando: [Cheatsheet do git-flow](http://danielkummer.github.io/git-flow-cheatsheet/index.pt_BR.html).

## Iniciar um projeto

Dado que é um projeto que será iniciado do zero:

```bash
cd <repository_directory>
git init
git remote add origin <repository_url>
git push -u origin master
git checkout -b develop
git push -u origin develop

```

## Iniciando uma nova _feature_

```bash
git checkout -b feature-<short_description>
```

Exemplo:

  * Se a branch atual for a develop: `git checkout -b feature-create_ads_tab`
  * Se a branch atual não for a develop: `git checkout -b feature-create_ads_tab develop`

## Finalizando uma _feature_

```bash
git checkout develop
git merge --no-ff feature-short_description
```

## Finalizando uma sprint

Caso seja necessário realizar ajustes ou correções que não configurem uma nova _feature_ antes do lançamento de uma versão crie uma branch relase para isso:

```bash
git checkout -b release-<version>
```

Exemplo:

  * Se a branch atual for a develop: `git checkout -b release-1.2.3`
  * Se a branch atual não for a develop: `git checkout -b release-1.2.3 develop`

> Observação: Nós sempre seguiremos um padrão para a numeração de versão dos aplicativos. Esse padrão é MAJOR.MINOR.MAINTENANCE
> Onde:
> 
>  * MAJOR: É incrementado para novas grandes lançamentos com mudanças drásticas.
>  * MINOR: É incrementado adição de novas funcionalidades pontuais.
>  * MAINTENANCE: É incrementado para a correção de problemas e pequenos ajustes em versões MINORs.

## Finalizando uma release

  1. Crie um pull request do branch com os últimos commits (deve ser da branch release caso exista, ou caso contrário da branch develop) para o master;
  2. Após o pull request ter sido aceito sigua os passos:

```bash
git checkout master
git pull
git tag -a v1.2.3 -m "Small message description" master
git push --tags
git checkout develop
```

Caso você tenha criado a uma branch release (exemplo: release-1.2.3):

```bash
git merge --no-ff release-1.2.3
git branch -d release-1.2.3
```

## Corringindo em uma versão

Supondo que ocorreu um problema na versão 1.2.3

```bash
git checkout tags/v1.2.3
git checkout -b hotfix-1.2.4

ou

git checkout -b hotfix-1.2.4 tags/v1.2.3
```

> Note que foi incrementado o número MAINTENANCE, por se tratar de uma correção.

## Finalizando um hotfix

  1. Crie um pull request do branch `hotfix` para o master;
  2. Após o pull request ter sido aceito sigua os passos:

```bash
git checkout master
git pull
git tag -a v1.2.4 -m "Small message description" master
git push --tags
git checkout develop
git merge --no-ff hotfix-1.2.4
git branch -d hotfix-1.2.4
```

> Observação: Lembre de apagar os branchs criados do repositório remoto após terminar de usa-los.



















