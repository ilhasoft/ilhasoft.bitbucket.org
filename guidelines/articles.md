# Artigos que recomendamos

# Android
* [Lista com canais de conhecimento, podcasts, bibliotecas, links, metodologias, ferramentas de debug e vários itens que agilizam o desenvolvimento Android](https://medium.com/android-news/compile-android-best-1-0-0-5f8b470d0ad9#.unzmg65ez)

# Swift

* [Introducing Stack Views](https://www.raywenderlich.com/114552/uistackview-tutorial-introducing-stack-views)
* [Swift Guard Statement - Why you should use it](http://ericcerney.com/swift-guard-statement/)
* [Learn how to document your project](http://nshipster.com/swift-documentation/)

## StoreKit

* [In-App Purchase Tutorial: Getting Started](https://www.raywenderlich.com/122144/in-app-purchase-tutorial)
* [Consumables and Receipt Validation](https://www.raywenderlich.com/23266/in-app-purchases-in-ios-6-tutorial-consumables-and-receipt-validation)
* [In-App Purchases: Non-Renewing Subscription](https://www.raywenderlich.com/36270/in-app-purchases-non-renewing-subscription-tutorial)
