# Ilhasoft Guidelines

A Ilhasoft é uma empresa que tem em seu core o desenvolvimento de aplicativos mobile. Prezamos sempre pela qualidade e agilidade no desenvolvimento, e pra isso criamos nossos aplicativos utilizando `tecnologias nativas` seguindo uma guideline própria de desenvolvimento, assim como a utilização de bibliotecas próprias e de terceiros. Nossas bibliotecas internas garantem código testado e estável que é compartilhado em alguns de nossos aplicativos.

Nossa empresa também desenvolve sistemas `back-end` que servem de apoio e gerenciamento aos aplicativos, para isso usamos tecnologias e frameworks de desenvolvimento ágil, como o `Django`.

## Android

Na plataforma Android utilizamos `Java` ou `Kotlin` como linguagens base para o desenvolvimento e temos como critério o uso do `API Level 16` como `minSdkVersion`.

Usamos o `Android Studio` como IDE para construir os aplicativos. Todo o nosso conhecimento adquirido com essa ferramenta (plugins, melhores práticas e etc) compartilhamos na seguinte [documentação](http://www.google.com.br).

O desenvolvedor deve usar como guia a nossa [convenção e padronização de código](http://www.google.com.br) para a criação de apps no padrão Ilhasoft. Nessa documentação, pode ser encontrada a arquitetura dos apps, assim como organização de pacotes, nomenclatura de variáveis e classes.

As bibliotecas que usamos estão listadas e descritas na seguinte [documentação](http://www.google.com.br). Além disso alguns artigos e recomendações de tecnologias podem ser encontradas no seguinte [endereço](http://ilhasoft.bitbucket.org/guidelines/#!articles.md).

Algumas regras de segurança que devem ser seguidas no decorrer do desenvolvimento. Podem ser encontradas nesse [endereço](http://ilhasoft.bitbucket.org/guidelines/#!keys-management.md)

## iOS

Sabemos que pode ser difícil mergulhar de cabeça no mundo iOS. A estrada pode ser longa e tortuosa da construção até o lançamento dos aplicativos na AppStore. Mas não se desespere! Foi por isso que criamos este documento. Ele tem como objetivo ajudar aos iniciantes e servir como guia aos desbravadores que sempre pensam e procuram fazer as coisas da "maneira correta". Tudo que escrevemos aqui são sugestões de boas práticas, mas caso tenha uma boa razão para fazer as coisas de maneira diferente, te encorajamos a seguir em frente!

[Boas Práticas no Desenvolvimento iOS](ios/coding-ios.md)

## Cloud

Quando criamos um novo aplicativo usamos como critério a abstração da camada de `API` com tecnologias como [`Parse`](https://www.parse.com/docs) e [`Firebase`](http://firebase.google.com/docs/). Em alguns casos, a depender do projeto, é necessário criar a camada de API sem a utilização desses frameworks, nesse caso criamos RESTs que utilizam `JSON` para troca de informação.

É de extrema importância que o desenvolvedor conheça todas essas plataformas, visto que, a escolha delas se dá durante o processo de desenvolvimento em uma reunião entre gerente e equipe técnica.

Quanto à utilização dessas ferramentas, temos um [manual de boas práticas](http://www.google.com.br) que deve ser seguido.
